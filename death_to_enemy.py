import asyncio
import aiohttp
timeout = aiohttp.ClientTimeout(total=5)
TARGETS = ["lenta.ru","ria.ru","rt.com","kremlin.ru","smotrim.ru","tass.ru","tvzvezda.ru","vsoloviev.ru","1tv.com","vgtrk.ru","zakupki.gov.ru","vesti.ru","online.sberbank.ru","sberbank.ru","vtb.ru","duma.gov.ru","rtr-planeta.com","5-tv.ru","rkn.gov.ru","rg.ru","government.ru ","data.gov.ru ","mchs.gov.ru","ac.gov.ru ","svr.gov.ru","gov.ru","council.gov.ru","premier.gov.ru","minenergo.gov.ru","economy.gov.ru","edu.gov.ru","torgi.gov.ru ","chechnya.gov.ru ","gosuslugi.ru","epp.genproc.gov.ru","ach.gov.ru","scrf.gov.ru","mil.ru"]

async def get(url, session):
    try:
        async with session.get(url=url) as response:
            resp = await response.read()
            print("Successfully got url {} with resp of length {}.".format(url, len(resp)))
    except Exception as e:
        print("Unable to get url {} due to {}.".format(url, e.__class__))


async def main(urls):
    async with aiohttp.ClientSession(timeout=timeout) as session:
        ret = await asyncio.gather(*[get('https://'+ url, session) for url in urls])
    print("Finalized all. Return is a list of len {} outputs.".format(len(ret)))


asyncio.run(main(TARGETS))
