FROM python:3.9-buster
WORKDIR /app
RUN apt-get update && apt-get install bash -y
ADD . /app
RUN pip install -r requirements.txt
RUN chmod +x entrypoint.sh
ENTRYPOINT [ "/app/entrypoint.sh" ]
